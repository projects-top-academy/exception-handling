﻿#include <iostream>
#include <stdexcept>
#include <fstream>

using namespace std;

// Задание 1

/*int main() {
	const int size = 10;
	int arr[size];

	try {
		for (int i = 0; i <= size; i++)
		{
			if (i == size)
			{
				throw out_of_range("Index out of range");
			}
			arr[i] = i;
		}
	}
	catch (const out_of_range& e) {
		cerr << "Exception caught: " << e.what() << "\n";
	}

	return 0;
}*/

//задание 2

int main() {
	ofstream file("nonexistent.txt");

	try {
		if (!file.is_open())
		{
			throw runtime_error("File not found or cannot be opened");
		}
		file << "Hello, World!";
	}
	catch (const exception& e) {
		cerr << "Exception caught: " << e.what() << "\n";
	}

	return 0;
}